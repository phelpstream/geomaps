const { get, isString } = require("svp")
const geocodeApi = require("./api/geocode")
const placeApi = require("./api/place")
const resultsApi = require("./api/results")

function geomaps(optionsOrApiKey) {
  this.apiKey = isString(optionsOrApiKey) ? optionsOrApiKey : get(optionsOrApiKey, "apiKey")
  return Object.assign({}, geocodeApi(this.apiKey), placeApi(this.apiKey), resultsApi)
}

module.exports = geomaps
