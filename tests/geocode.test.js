let apiKey = process.env.API_KEY || null
const fs = require("fs")
console.log("apiKey", apiKey)
const { getAddresses, getAddressesByLatLng } = require("../api/geocode")(apiKey)
const { sortByTypes, filterByType } = require("../api/results")
if (apiKey) {
  getAddresses("").then(results => {
    console.log("results", JSON.stringify(results, null, 2))
  })
  // getAddressesByLatLng(68.381562, 23.55847940000001).then(results => {
  //   fs.writeFileSync("./resultsByTypes.json", JSON.stringify(filterByType("street_address", results), null, 2))
  // })
} else {
  console.error("No api key.")
}
