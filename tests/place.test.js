let apiKey = process.env.API_KEY || null

const { get } = require("svp")

const fs = require("fs")
// console.log("apiKey", apiKey)
const { getPlacesNearby, getPlaceDetails } = require("../api/place")(apiKey)
const { sortByTypes } = require("../api/results")
if (apiKey) {
  getPlacesNearby(46.168091, 1.884174, "PARAMEDIC 23").then(results => {
    getPlaceDetails(get(results, "0.place.id"), { debug: true })
      .then(results => {
        fs.writeFileSync("../tests/placeDetails.json", JSON.stringify(results, null, 2))
      })
      .catch(error => console.error(error))
    // fs.writeFileSync("../tests/placesNearby.json", JSON.stringify(sortByTypes(results), null, 2))
  })
} else {
  console.error("No api key.")
}
