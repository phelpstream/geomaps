const { GEOCODE_API_ENDPOINT } = require("../config.json").ENDPOINTS
const { partial, each, get } = require("svp")

module.exports = function(apiKey) {
  const { fetchParsedResults } = require("./fetch")(apiKey)

  function getAddresses(apiKey, freeFormAddress, options = {}) {
    return fetchParsedResults(
      GEOCODE_API_ENDPOINT,
      {
        address: freeFormAddress
      },
      options
    )
  }

  function getAddressesByLatLng(apiKey, latitude, longitude, options = {}) {
    return fetchParsedResults(
      GEOCODE_API_ENDPOINT,
      {
        latlng: `${`${latitude}`.trim()},${`${longitude}`.trim()}`
      },
      options
    )
  }

  return {
    getAddresses: partial(getAddresses, apiKey),
    getAddressesByLatLng: partial(getAddressesByLatLng, apiKey)
  }
}
