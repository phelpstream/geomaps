const { PLACE_NEARBY_API_ENDPOINT, PLACE_DETAILS_API_ENDPOINT } = require("../config.json").ENDPOINTS
const { GEOCODE_API_ENDPOINT } = require("../config.json").ENDPOINTS
const { partial, each, get } = require("svp")

module.exports = function(apiKey) {
  const { fetchParsedResults } = require("./fetch")(apiKey)

  function getPlacesNearby(apiKey, latitude, longitude, keyword = "", radius = 1000, options = {}) {
    return fetchParsedResults(
      PLACE_NEARBY_API_ENDPOINT,
      {
        location: `${`${latitude}`.trim()},${`${longitude}`.trim()}`,
        radius: radius,
        keyword: keyword
      },
      options
    )
  }

  function getPlaceDetails(apiKey, placeId, options = {}) {
    return fetchParsedResults(
      PLACE_DETAILS_API_ENDPOINT,
      {
        placeid: placeId
      },
      options
    )
  }

  return {
    getPlacesNearby: partial(getPlacesNearby, apiKey),
    getPlaceDetails: partial(getPlaceDetails, apiKey)
  }
}
