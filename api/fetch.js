const { partial, each, get, isObject, isArray, assign, has } = require("svp")

const { parseResult } = require("../utils/parse")

const qs = require("querystring")
const axios = require("axios")

function getOptions(options) {
  return Object.assign(
    {
      format: "json",
      reqOptions: {}
    },
    options
  )
}

function buildUrl(url, format, query, key = "") {
  if (!has(query, "key")) query.key = key
  return `${url}/${format}?${qs.stringify(query)}`
}

function fetchData(url, options = {}) {
  let axiosParams = assign(
    {},
    {
      url: url,
      method: "get"
    },
    options
  )
  return axios(axiosParams).then(response => {
    let status = get(response, "data.status")
    if (status === "OK") {
      let resultData = get(response, "data.results", [])
      if (has(response, "data.result")) resultData = get(response, "data.result", {})
      return {
        ok: true,
        data: resultData
      }
    } else if (status === "ZERO_RESULTS") {
      return {
        ok: true,
        data: []
      }
    } else {
      return {
        ok: false,
        status: status,
        error: get(response, "data.error_message")
      }
    }
    return null
  })
}

// let postData = (url, options) => request(url, assign({}, options, { method: "post" }))

function fetchResults(apiKey, urlEndpoint, query = {}, options = {}) {
  let { format, reqOptions } = getOptions(options)
  let requestUrl = buildUrl(urlEndpoint, format, query, apiKey)
  return fetchData(requestUrl, reqOptions).then(({ ok, data, status, error }) => {
    if (ok) {
      return { data }
    } else {
      return { error }
      // throw new Error(`Error fetching Google API with status: ${status} and message: ${error}`)
    }
  })
}

function fetchParsedResults(apiKey, urlEndpoint, query = {}, options = {}) {
  return fetchResults(apiKey, urlEndpoint, query, options).then(({ data, error }) => {
    // console.log("results", JSON.stringify(results, null, 2))
    if (data) {
      if (isObject(data)) {
        return parseResult(data, {
          debug: get(options, "debug", false)
        })
      } else if (isArray(data)) {
        return each(data, (result, index) =>
          parseResult(result, {
            debug: get(options, "debug", false)
          })
        )
      }

      return data
    }
    if (error) {
      console.error("Return null because:", error)
      return null
    }
    return null
  })
}

module.exports = function(apiKey) {
  return {
    fetchResults: partial(fetchResults, apiKey),
    fetchParsedResults: partial(fetchParsedResults, apiKey)
  }
}
