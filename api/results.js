const { each, get, hasnt, isArray } = require("svp")

module.exports = {
  filterByType: (type, results) => {
    return results.filter(result => result.google.types.includes(type))
  },
  sortByTypes: results => {
    let sortedResults = {
      types: {},
      total: results.length
    }
    each(results, result => {
      let types = get(result, "google.types", [])
      each(types, type => {
        if (hasnt(sortedResults.stypes, type)) sortedResults.types[type] = []
        sortedResults.types[type].push(result)
      })
    })
    return sortedResults
  }
}
