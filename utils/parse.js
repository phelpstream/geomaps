const { assign, get, each, equal, isString, isArray } = require("svp")
const { ADDRESS_COMPONENTS_MAP } = require("../config.json")

function parseAddressComponents(addressComponents) {
  let outputAddress = {}
  // console.log("addressComponents", JSON.stringify(addressComponents, null, 2))
  each(ADDRESS_COMPONENTS_MAP, (targetTypes, addressKey) => {
    let foundAddressProperty = addressComponents.find(ac => {
      if (isString(targetTypes)) return ac.types.includes(targetTypes)
      if (isArray(targetTypes)) return equal(ac.types, targetTypes)
      return false
    })
    // console.log("foundAddressProperty", JSON.stringify(foundAddressProperty, null, 2))
    if (foundAddressProperty) {
      let shortName = get(foundAddressProperty, "short_name")
      let longName = get(foundAddressProperty, "long_name")
      if (longName) outputAddress[addressKey] = longName
      if (shortName && longName && shortName.length < longName.length) outputAddress[`${addressKey}Code`] = shortName
    }
  })

  return outputAddress
}

function parseResult(result, { debug = false } = {}) {
  // console.dir(result);
  let address_components = get(result, "address_components", [])
  // console.dir(address_components);
  // console.log("address_components", JSON.stringify(address_components, null, 2))
  let addressComponents = parseAddressComponents(address_components)
  let debugData = debug ? result : { address_components }

  // console.log("addressComponents", JSON.stringify(addressComponents, null, 2))
  return {
    name: get(result, "name", null),
    address: assign({}, addressComponents),
    position: {
      latitude: get(result, "geometry.location.lat", null),
      longitude: get(result, "geometry.location.lng", null),
      viewport: {
        northeast: {
          latitude: get(result, "geometry.viewport.northeast.lat", null),
          longitude: get(result, "geometry.viewport.northeast.lng", null)
        },
        southwest: {
          latitude: get(result, "geometry.viewport.southwest.lat", null),
          longitude: get(result, "geometry.viewport.southwest.lng", null)
        }
      }
    },
    phone: {
      formatted: get(result, "formatted_phone_number", null),
      international: get(result, "international_phone_number", null)
    },
    website: get(result, "website", null),
    photos: get(result, "photos", []),
    place: {
      id: get(result, "place_id", null),
      altIds: get(result, "alt_ids", []),
      rating: get(result, "rating", null),
      reviews: get(result, "reviews", []),
      priceLevel: get(result, "price_level", null),
      url: get(result, "url", null),
      permanentlyClosed: get(result, "permanently_closed", null)
    },
    openingHours: get(result, "opening_hours", null),
    formattedAddress: get(result, "formatted_address", null),
    google: {
      id: get(result, "id", null),
      reference: get(result, "reference", null),
      icon: get(result, "icon", null),
      types: get(result, "types", [])
    },
    _debugData: debugData
  }
}

module.exports = {
  parseResult
}
